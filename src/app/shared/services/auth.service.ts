import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public returnUrl: string = '';
  public queryParams: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  public autentication(obj: any) {
    if (obj.userName === 'user' && obj.password === '123') {
      this.router.navigateByUrl('/usersList');
    }
  }
}
