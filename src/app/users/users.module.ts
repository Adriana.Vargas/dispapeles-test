import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersListPageComponent } from './containers/users-list-page/users-list-page.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from '../shared/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UsersService } from './services/users.service';
import { UserCreateDialogComponent } from './components/user-create-dialog/user-create-dialog.component';
import { UserCreateDialogPageComponent } from './containers/user-create-dialog-page/user-create-dialog-page.component';

const routes: Routes = [
  {
    path: '',
    component: UsersListPageComponent,
  }
];

@NgModule({
  declarations: [
    UsersListComponent,
    UsersListPageComponent,
    UserCreateDialogComponent,
    UserCreateDialogPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [UsersService]
})
export class UsersModule { }
