export interface User {
    apellidos: string;
    createdAt: string;
    direccion: string;
    edad: number;
    id: number;
    identificacion: string;
    nombre: string;
    origen: string;
    telefono: string;
    tipoId: string;
}