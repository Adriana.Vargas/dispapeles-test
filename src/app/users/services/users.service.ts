import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private baseUrl = 'https://632c95941aabd837399f4600.mockapi.io/api/v1';

  constructor(private http: HttpClient) { }


  public getAllUsers() {
    return this.http.get(`${this.baseUrl}/person`);
  }

  public getTypeId() {
    return this.http.get(`${this.baseUrl}/tipoidentificacion`);
  }

  public saveUser(user: User) {
    return this.http.post(`${this.baseUrl}/person`, user);
  }

  public deleteUser(user: User) {
    return this.http.delete(`${this.baseUrl}/person/${user.id}`);
  }

  public updateUser(user: User) {
    return this.http.put(`${this.baseUrl}/person/${user.id}`, user);
  }
}
