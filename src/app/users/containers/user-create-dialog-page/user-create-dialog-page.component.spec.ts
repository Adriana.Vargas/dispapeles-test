import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserCreateDialogPageComponent } from './user-create-dialog-page.component';

describe('UserCreateDialogPageComponent', () => {
  let component: UserCreateDialogPageComponent;
  let fixture: ComponentFixture<UserCreateDialogPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserCreateDialogPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserCreateDialogPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
