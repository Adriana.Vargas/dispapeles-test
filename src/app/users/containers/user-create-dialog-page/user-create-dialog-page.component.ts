import { Component, Inject, OnInit } from '@angular/core';
import { User } from '../../models/user.model';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { UsersService } from '../../services/users.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-user-create-dialog-page',
  templateUrl: './user-create-dialog-page.component.html',
  styleUrls: ['./user-create-dialog-page.component.scss']
})
export class UserCreateDialogPageComponent implements OnInit {
  public saveSuccess: boolean = false;

  constructor(
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private usersService: UsersService,
    @Inject(MAT_DIALOG_DATA) public data: { isEdition: boolean, typeIdList: any, dataLength: number, user?: User },
  ) { }

  ngOnInit(): void {
  }

  public saveUser(user: User) {
    this.usersService.saveUser(user).subscribe((res: any) => {
      this.dialog.closeAll();
      this.snackBar.open('Usuario creado!', 'Cerrar', {
        duration: 2000
      });
    });
  }

  public updateUser(user: User) {
    this.usersService.updateUser(user).subscribe((res:any) => {
      this.dialog.closeAll();
      this.snackBar.open('Usuario Editado!', 'Cerrar', {
        duration: 2000
      });
    })
  }


}
