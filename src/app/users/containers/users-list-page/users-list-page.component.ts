import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from '../../models/user.model';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-users-list-page',
  templateUrl: './users-list-page.component.html',
  styleUrls: ['./users-list-page.component.scss']
})
export class UsersListPageComponent implements OnInit {
  public userListData: User[] = [];
  public typeIdList: any[] = [];

  constructor(
    private snackBar: MatSnackBar,
    private usersService: UsersService) {
   }

  ngOnInit(): void {
    this.getUserListData();
    this.getTypeId();
  }

  public getUserListData() {
    this.usersService.getAllUsers().subscribe((res: any) => {
      this.userListData = res;
    });
  }

  public getTypeId() {
    this.usersService.getTypeId().subscribe((res: any) => {
        this.typeIdList = res;
    });
  }

  public deleteUser(user: User) {
    this.usersService.deleteUser(user).subscribe((res:any) => {
      this.snackBar.open('Usuario eliminado!', 'Cerrar', {
        duration: 2000
      });
    })
  }
}
