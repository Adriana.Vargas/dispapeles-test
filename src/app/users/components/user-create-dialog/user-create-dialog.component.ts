import { Component, Inject, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { User } from '../../models/user.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-create-dialog',
  templateUrl: './user-create-dialog.component.html',
  styleUrls: ['./user-create-dialog.component.scss']
})
export class UserCreateDialogComponent implements OnInit {
  public userForm!: FormGroup;
  public typeIdList: any;
  @Input() data: any;
  @Input() saveSuccess: boolean = false;
  @Output() saveUser: EventEmitter<User> = new EventEmitter<User>();
  @Output() updateUser: EventEmitter<User> = new EventEmitter<User>();

  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.typeIdList = this.data.typeIdList || [];
    this.userForm = this.fb.group({
      apellidos: [this.data && this.data.user && this.data.user.apellidos || '', [Validators.required]],
      createdAt: [this.data && this.data.user && this.data.user.createdAt || '',],
      direccion: [this.data && this.data.user && this.data.user.direccion || '', [Validators.required]],
      edad: [this.data && this.data.user && this.data.user.edad || '', [Validators.required]],
      id: [this.data && this.data.user && this.data.user.id || (this.data.dataLength + 1).toString()],
      identificacion: [this.data && this.data.user && this.data.user.identificacion || '', [Validators.required]],
      nombre: [this.data && this.data.user && this.data.user.nombre || '', [Validators.required]],
      origen: [this.data && this.data.user && this.data.user.origen || ''],
      telefono: [this.data && this.data.user && this.data.user.telefono || '', [Validators.required]],
      tipoId: [this.data && this.data.user && this.data.user.id || null, [Validators.required]],
    });
  }

  public addUser(user: User) {
    if(this.data.isEdition) this.updateUser.emit(user);
    else this.saveUser.emit(user);
  }

  public numberValidation(event: any) {
    return event.charCode >= 48 && event.charCode <= 57
  }

}
