import { Component, Input, OnInit, Output, SimpleChanges, ViewChild, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { User } from '../../models/user.model';
import { UserCreateDialogComponent } from '../user-create-dialog/user-create-dialog.component';
import { UserCreateDialogPageComponent } from '../../containers/user-create-dialog-page/user-create-dialog-page.component';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  public displayedColumns: string[] = ['idType', 'id', 'name', 'lastName', 'age', 'phone', 'direction', 'actions'];
  public dataSource: MatTableDataSource<any> = new MatTableDataSource<any>([]);
  @Input() userListData: User[] = [];
  @Input() typeIdList: any = [];
  @Output() onDeleteUser: EventEmitter<User> = new EventEmitter<User>();

  private dialogRefDialogComponent!: MatDialogRef<UserCreateDialogPageComponent, any>;
  
  constructor(
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.userListData) this.dataSource.data = this.userListData;
  }

  public openCreateUserDialog(isEdition: boolean, user?: User) {
    const typeIdList = this.typeIdList;
    const dataLength = this.dataSource.data.length
    this.dialogRefDialogComponent = this.dialog.open(UserCreateDialogPageComponent, {
      width: '60%',
      data: {
        isEdition,
        typeIdList,
        user,
        dataLength
       }
    });
    this.dialogRefDialogComponent.afterClosed().subscribe(result => {
      if ( result !== '' && !isEdition) this.dataSource.data = [...this.dataSource.data, result];
      if ( result !== '' && isEdition) {
        this.dataSource.data.find((element, index) => {
          if (element.id === result.id) {
            this.dataSource.data[index] = result;
            this.dataSource._updateChangeSubscription();
          }
        });
      }
    });
  }

  public deleteUser(user: User, i: number) {
    this.dataSource.data.splice(i, 1);
    this.dataSource._updateChangeSubscription();
    this.onDeleteUser.emit(user);
  }

}
